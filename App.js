import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './components/Home';
import Detail from './components/Detail';

const Stack = createStackNavigator();

const App = () => {
  const options = {
    headerStyle: {
      backgroundColor: '#008080',
      fontFamily: 'Helvetica',
    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: '700',
    },
  };

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={Home}
          options={{ ...options, title: 'Inicio' }}
        />
        <Stack.Screen
          name="Detail"
          component={Detail}
          options={{ ...options, title: 'Detalle' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
