import React, { useState, useEffect } from 'react';
import {
  Animated,
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
} from 'react-native';

const Post = ({ post }) => {
  post = post.item;
  return (
    <View style={styles.post}>
      {/** Se usa el concepto de flexbox para alinear los componentes de la imagen y descripcion */}
      <View style={styles.post_image}>
        {/** Image utiliza la propiedad picture de item para usarla en el atriburto source y mostrar la imagen  */}
        <Image
          resizeMode={'cover'}
          source={{
            uri: post.image,
          }}
          style={styles.post_id_image}
        />
      </View>
      <View style={styles.post_content}>
        {/** Se usan las propiedades de item para mostrar titulo, nombre, apellidos y correo (Descripcion) */}
        <Text style={styles.post_content_text}>
          <Text style={styles.post_content_title_text}>Titlulo: </Text>
          {post.text}
        </Text>
        <Text>
          <Text style={styles.post_content_title_text}>Publicación: </Text>
          {post.publishDate}
        </Text>
        <Text>
          <Text style={styles.post_content_title_text}>Likes: </Text>
          {post.likes}
        </Text>
      </View>
    </View>
  );
};

const Detail = ({ route }) => {
  const { item, datosApi } = route.params;
  const [posts, setPosts] = useState([]);

  //USo del Hook UseEffect, para obtener los datos por primera vez
  useEffect(() => {
    //Se usa fetch para poder hacer la peticion al API
    //Se pasa la URL y los datos necesarios como el metodo y header que tiene el APP_ID generado en el registro
    fetch(datosApi.baseURL + '/user/' + item.id + '/post', {
      method: 'GET',
      headers: { 'app-id': datosApi.idAPI },
      mode: 'cors',
    })
      .then((res) => res.json())
      .then((res) => {
        //Se setea el estado del array usuarios con el request de la peticion
        setPosts(res.data);
      });
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <Image source={{ uri: item.picture }} style={styles.image} />
        <Text style={styles.title}>
          {item.title + ' ' + item.firstName + ' ' + item.lastName}
        </Text>
        <Text style={styles.text}>{item.email}</Text>
        <Text style={styles.title}> Posts</Text>
        <FlatList
          data={posts}
          renderItem={(post) => <Post post={post} />}
          keyExtractor={(post) => post.id}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  image: {
    width: 250,
    height: 300,
    alignSelf: 'center',
    borderRadius: 8,
  },
  container: {
    marginTop: 20,
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    paddingHorizontal: 10,
    fontFamily: 'Helvetica',
  },
  title: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 30,
    marginTop: 5,
    marginBottom: 5,
    textTransform: 'capitalize',
  },
  text: {
    textAlign: 'center',
    fontSize: 20,
  },

  post_id_image: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
  },
  post: {
    flex: 1,
    flexDirection: 'row',
    margin: 5,
    borderRadius: 10,
    borderWidth: 4,
    borderColor: '#008080',
    borderStyle: 'solid',
    overflow: 'hidden',
  },
  post_image: {
    flex: 0.3,
  },
  post_content: {
    flex: 0.7,
    padding: 10,
  },
  post_content_text: {
    textTransform: 'capitalize',
  },
  post_content_title_text: {
    fontWeight: 'bold',
  },
});

export default Detail;
