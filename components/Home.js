import React, { useState, useEffect } from 'react';
import {
  Animated,
  Text,
  View,
  StyleSheet,
  StatusBar,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  Button,
} from 'react-native';

//DUMM API - devuelve una serie de datos en json
const datosApi = {
  baseURL: 'https://dummyapi.io/data/api', //Url base de la api
  idAPI: '6069741bfe0a0f36017d0e16', //llave para la peticion (500 consultas/dia)
};

const Item = ({ item, datosApi, navigation }) => {
  return (
    <View style={styles.item}>
      {/** Se usa el concepto de flexbox para alinear los componentes de la imagen y descripcion */}
      <View style={styles.item_image}>
        {/** Image utiliza la propiedad picture de item para usarla en el atriburto source y mostrar la imagen  */}
        <Image
          resizeMode={'cover'}
          source={{
            uri: item.picture,
          }}
          style={styles.image}
        />
      </View>
      <View style={styles.item_content}>
        {/** Se usan las propiedades de item para mostrar titulo, nombre, apellidos y correo (Descripcion) */}
        <Text style={styles.item_content_text}>
          <Text style={styles.item_content_title_text}>Nombre: </Text>
          {item.title + ' ' + item.firstName + ' ' + item.lastName}
        </Text>
        <Text>
          <Text style={styles.item_content_title_text}>E-mail: </Text>
          {item.email}
        </Text>
        <View style={styles.item_button}>
          <Button
            title="Ver detalles"
            color="#008080"
            onPress={() =>
              navigation.navigate('Detail', {
                item: item,
                datosApi: datosApi,
              })
            }
          />
        </View>
      </View>
    </View>
  );
};

const Home = ({ navigation }) => {
  //Uso del Hook UseState, la variable usuarios contendrá una coleccion (array) de elementos
  const [usuarios, setUsuarios] = useState([]);

  //USo del Hook UseEffect, para obtener los datos por primera vez
  useEffect(() => {
    //Se usa fetch para poder hacer la peticion al API
    //Se pasa la URL y los datos necesarios como el metodo y header que tiene el APP_ID generado en el registro
    fetch(datosApi.baseURL + '/user', {
      method: 'GET',
      headers: { 'app-id': datosApi.idAPI },
      mode: 'cors',
    })
      .then((res) => res.json())
      .then((res) => {
        //Se setea el estado del array usuarios con el request de la peticion
        setUsuarios(res.data);
      });
  }, []);

  //funcion que se encarga de renderizar el componente Item, pasandole como props (item) el valor proporcionado
  const renderItem = ({ item }) => {
    return <Item item={item} datosApi={datosApi} navigation={navigation} />;
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Usuarios</Text>
      {/* FlatList permite reenderizar listas*/}
      {/* El atributo data contiene la lista*/}
      {/* El atributo renderItem contiene una funcion que renderiza cada item en un componente o contenedor*/}
      <FlatList
        data={usuarios}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
};

//Stylesheet para el uso de estilos
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    fontFamily: 'Helvetica',
  },
  title: {
    alignItems: 'center',
    textAlign: 'center',
    padding: 10,
    fontSize: 25,
    fontWeight: 'bold',
    color: '#008080',
  },
  image: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    margin: 5,
    borderRadius: 10,
    borderWidth: 4,
    borderColor: '#008080',
    borderStyle: 'solid',
    overflow: 'hidden',
  },
  item_image: {
    flex: 0.3,
  },
  item_content: {
    flex: 0.7,
    padding: 10,
  },
  item_button: {
    alignItems: 'center',
    marginTop: 10,
  },
  item_content_text: {
    textTransform: 'capitalize',
  },
  item_content_title_text: {
    fontWeight: 'bold',
  },
});

export default Home;
